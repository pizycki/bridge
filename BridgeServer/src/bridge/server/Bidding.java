package bridge.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import bridge.server.bids.Bid;
import bridge.server.bids.Colour;
import bridge.server.bids.Kontra;
import bridge.server.bids.NormalBid;
import bridge.server.bids.Pas;
import bridge.server.bids.Rekontra;

public class Bidding {

	public static final int DEFAULT_SERVER_SOCKET_PORT = 5555;
	private static final String[] PLAYER_NAMES = new String[] { "N", "E", "S",
			"W" };

	protected final List<Player> players;
	private int serverSocketPort;
	private boolean biddingContinues;
	private ServerSocket serverSocket;
	private boolean initialized;
	private Bid highestBid;

	private int kontraCounter;
	private boolean kontraNS;
	private boolean kontraWE;
	private boolean rekontraNS;
	private boolean rekontraWE;

	public Bidding() {
		this.players = new ArrayList<Player>();
		this.serverSocketPort = DEFAULT_SERVER_SOCKET_PORT;
		this.initialized = false;

	}

	public Bidding(int port) {
		this();
		this.serverSocketPort = port;
	}

	public void init() {
		this.initialized = false;

		// Create and start new instance of server socket
		createServerSocket();
		writeLine("Server socket created!");

		// Prepare list of players
		resetPlayers();
		writeLine("Players list ready");

		// Wait for players to subscribe. Each player has it's own port.
		subscribePlayers();
		writeLine("All 4 players subscribed");

		this.highestBid = null;

		// Kontry
		this.kontraCounter = 0;
		this.kontraNS = false;
		this.kontraWE = false;
		// Rekontry
		this.rekontraNS = false;
		this.rekontraWE = false;

		// It's fresh new bidding so it must continues.
		biddingContinues = true;

		this.initialized = true;
		writeLine("Bidding successfuly initialized!");
	}

	public void start() {
		if (this.initialized) {
			// Notify all players about game start
			for (Player player : this.players) {
				SocketHelper.SendData(Commands.START, player.getSocket(), true);
			}

			// Start binding
			Player[] biddingPlayers = this.players
					.toArray(new Player[this.players.size()]);
			int loopCounter = 0;
			int pasCounter = 0;
			int currBiddingPlayerIdx;
			while (biddingContinues) {
				boolean nextBidder = true;
				currBiddingPlayerIdx = loopCounter % 4;
				Player bidder = biddingPlayers[currBiddingPlayerIdx];
				String clientSentence = "";

				BufferedReader inFromClient;

				SocketHelper.SendData(Commands.TURN, bidder.getSocket(), true);
				try {
					inFromClient = new BufferedReader(new InputStreamReader(
							bidder.getSocket().getInputStream()));
					writeLine("Waiting for player " + bidder.getName());
					clientSentence = inFromClient.readLine();
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException("Error");
				}

				// Request cannot be empty
				if (clientSentence.equals("")) {
					SocketHelper.SendData(Commands.INVALID, bidder.getSocket(),
							true);
					nextBidder = false;
					continue;
				}

				// TODO LOGIC here

				Bid bid = parse(clientSentence);
				if (bid == null) {
					// Invalid bid request
					SocketHelper.SendData(Commands.INVALID, bidder.getSocket(),
							true);
					nextBidder = false;
					continue;
				}

				// Check type
				if (bid instanceof Pas) {
					pasCounter++;
					if (pasCounter == 3 || pasCounter == 4) {
						// Finish the game
						Player winner;
						if (this.highestBid == null) {
							final Player noWinner = new Player();
							noWinner.setName("No one");
							// socket remains null
							winner = noWinner;
						} else {
							winner = biddingPlayers[(currBiddingPlayerIdx++ + 1) % 4];
						}
						raiseWinnerSelected(winner);
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						raiseGameOverEvent();
						biddingContinues = false;
						try {
							serverSocket.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						continue;
					}
				} else {
					pasCounter = 0;
				}

				if (bid instanceof NormalBid) {
					if (this.highestBid != null) {
						// Is greater than actual greatest bid?
						if (bid.getValue() <= this.highestBid.getValue()) {
							// Invalid bid request
							SocketHelper.SendData(Commands.INVALID,
									bidder.getSocket(), true);
							nextBidder = false;
							continue;
						} else {
							this.highestBid = bid;
						}
					} else {
						this.highestBid = bid;
					}
				} else if (bid instanceof Kontra) {
					boolean invalid = this.kontraNS || this.kontraWE
							|| this.rekontraNS || this.rekontraWE;
					if (!invalid) {
						if (bidder.getName().equals("N")
								|| bidder.getName().equals("S")) {
							this.kontraNS = true;
							this.kontraCounter = 3;
						} else {
							this.kontraWE = true;
							this.kontraCounter = 3;
						}
					}
					if (invalid) {
						// Invalid bid request
						SocketHelper.SendData(Commands.INVALID,
								bidder.getSocket(), true);
						nextBidder = false;
						continue;
					}
				} else if (bid instanceof Rekontra) {
					boolean invalid = (!this.kontraNS || !this.kontraWE)
							&& (this.rekontraNS && this.rekontraWE);

					if (!invalid) {
						if ((bidder.getName().equals("N") || bidder.getName()
								.equals("S")) && kontraWE == true) {
							this.rekontraNS = true;
							this.kontraCounter = 3;
						} else if ((bidder.getName().equals("W") || bidder
								.getName().equals("E"))
								&& this.kontraNS == true) {
							this.rekontraWE = true;
							this.kontraCounter = 3;
						} else {
							invalid = true;
						}
					}

					// dont replace with 'else'!

					if (invalid) {
						// Invalid bid request
						SocketHelper.SendData(Commands.INVALID,
								bidder.getSocket(), true);
						nextBidder = false;
						continue;
					}
				}

				// decrement kontraCounter
				this.kontraCounter--;
				if (kontraCounter == 0) {
					this.kontraNS = false;
					this.kontraWE = false;
					this.rekontraNS = false;
					this.rekontraWE = false;
				}

				// Check is this bid allowed

				// Inform players about the new bid
				bid.setPlayer(bidder);
				bid.setDetails(clientSentence);
				raiseNewBidEvent(bid);

				if (nextBidder) {
					loopCounter++;
				}
			}
		}
	}

	protected void raiseNewBidEvent(Bid bid) {
		// Inform all bidders about new bid
		if (players != null && !players.isEmpty()) {
			for (Player player : this.players) {
				if (player != null) {
					player.onNewBidEvent(bid);
				} else {
					throw new RuntimeException(
							"One of the subscribed players is null!");
				}
			}
		}
	}

	protected void raiseNewPlayerJoinedEvent(Player newPlayer) {
		if (newPlayer == null)
			throw new NullPointerException("New player instance is null");

		// Inform all players about joining of new player
		if (players != null && !players.isEmpty()) {
			for (Player player : this.players) {
				if (!newPlayer.equals(player)) {
					if (player != null) {
						player.onNewPlayerJoinEvent(newPlayer);
					} else {
						throw new RuntimeException(
								"One of the subscribed players is null!");
					}
				}
			}
		}
	}

	protected void raiseGameOverEvent() {
		// Inform all players about game over a
		if (players != null && !players.isEmpty()) {
			for (Player player : this.players) {
				if (player != null) {
					player.onGameOver();
				} else {
					throw new RuntimeException(
							"One of the subscribed players is null!");
				}
			}
		}
	}

	protected void raiseWinnerSelected(Player winner) {
		if (winner == null)
			throw new NullPointerException("Winner is null");

		// Inform all players about the winner
		if (players != null && !players.isEmpty()) {
			for (Player player : this.players) {
				if (player != null) {
					player.onWinnerSelectedEvent(winner);
				} else {
					throw new RuntimeException(
							"One of the subscribed players is null!");
				}
			}
		}
	}

	protected Bid parse(String request) {
		switch (request) {
		case "pas":
			return new Pas();
		case "kontra":
			return new Kontra();
		case "rekontra":
			return new Rekontra();
		default:
			// try to retrive colour and level
			if (request.length() > 3) {
				NormalBid bid = new NormalBid();

				// Take first character as the level
				int level;
				try {
					level = Integer.parseInt(request.substring(0, 1));
				} catch (NumberFormatException e) {
					return null;
				}

				if (level < 1 || level > 7) {
					return null;
				}

				bid.setLevel(level);

				// Take the rest as the colour
				String colour = request.substring(2).toLowerCase();
				switch (colour) {
				case "trefl":
					bid.setColour(Colour.TREFL);
					break;
				case "karo":
					bid.setColour(Colour.KARO);
					break;
				case "kier":
					bid.setColour(Colour.KIER);
					break;
				case "pik":
					bid.setColour(Colour.PIK);
					break;
				case "bez atu":
					bid.setColour(Colour.BEZ_ATU);
					break;
				default:
					return null;
				}
				return bid;
			}
		}
		return null;
	}

	private void createServerSocket() {
		if (this.serverSocket != null) {
			try {
				this.serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			this.serverSocket = new ServerSocket(DEFAULT_SERVER_SOCKET_PORT);
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port "
					+ DEFAULT_SERVER_SOCKET_PORT, e);
		}
	}

	private void resetPlayers() {
		if (this.players == null) {
			throw new RuntimeException(
					"Players list was not created. Use a proper constructor!");
		} else if (!players.isEmpty()) {
			this.players.clear();
		}
	}

	private void subscribePlayers() {
		// Wait for terminal to say 'hello'
		Socket clientSocket = null;
		int playerNr = 1;
		while (this.players.size() < 4) {
			try {
				writeLine("Awaiting player #" + playerNr + "...");
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				throw new RuntimeException(
						"IOException while accepting client connection", e);
			}
			if (clientSocket != null) {
				// Create new player with accepted connection
				Player p = new Player();
				p.setSocket(clientSocket);
				p.setName(PLAYER_NAMES[playerNr - 1]);
				this.players.add(p);

				writeLine("New player subscribed: " + p.toString());
				raiseNewPlayerJoinedEvent(p);

				String data = "Connected as player: "
						+ p.getName()
						+ ". "
						+ (playerNr != 4 ? " Waiting for other players..." : "")
						+ '\n';
				SocketHelper.SendData(data, clientSocket, false);

				playerNr++;
			}
		}
	}

	public static void writeLine(String line) {
		System.out.println(line);
	}
}
