package bridge.server.bids;

public class NormalBid extends Bid {

	public NormalBid() {
		super();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getColour() {
		return colour;
	}

	public void setColour(int colour) {
		this.colour = colour;
	}

	private int level;
	private int colour;

	@Override
	public int getValue() {
		return level * 10 + colour;
	}

}
