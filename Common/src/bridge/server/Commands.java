package bridge.server;

public class Commands {
	public static final String START = "START"; // when game has started
	public static final String OVER = "OVER"; // when game has started
	
	public static final String INVALID = "INVALID"; // when user must retype his choice
	
	public static final String TURN = "TURN"; // when its this players turn
	public static final String WAIT = "WAIT"; // when its another players turn

	public static final String PAS = "PAS";

	public static final String BEZ_ATU = "BEZ_ATU";
	public static final String KARO = "KARO";
	public static final String KIER = "KIER";
	public static final String TREFL = "TREFL";
	public static final String PIK = "PIK";

}
