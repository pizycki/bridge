package bridge.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import bridge.server.bids.Bid;

public class Player {
	private String name;
	private Socket socket;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", socket port=" + socket.getPort()
				+ "]";
	}

	public void onNewBidEvent(Bid bid) {
		// Inform the player about the bid
		if (bid == null)
			throw new NullPointerException("Bid instance is null");

		informPlayer("Player " + bid.getPlayer().getName() + ": "
				+ bid.getDetails());

	}

	public void onNewPlayerJoinEvent(Player newPlayer) {
		if (newPlayer == null)
			throw new NullPointerException("New player instance is null");

		informPlayer("New player joined the game! Player: "
				+ newPlayer.getName());
	}

	public void onWinnerSelectedEvent(Player winner) {
		if (winner == null)
			throw new NullPointerException("Winner instance is null");
		else {
			informPlayer("Bidding is over!! Winner is "
					+ (this.equals(winner) ? "you! Cogratz!" : winner.getName()));
		}
	}

	public void onGameOver() {
		informPlayer(Commands.OVER, true);

		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void informPlayer(String info) {
		informPlayer(info, false);
	}

	protected void informPlayer(String info, boolean silent) {
		SocketHelper.SendData(info, this.socket, silent);
	}
}
