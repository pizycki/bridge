package bridge.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketHelper {

	public static final char SILENT_MODE = 's';
	public static final char VERBAL_MODE = 'v';

	public static void SendData(String data, Socket socket, boolean silent) {
		DataOutputStream outToClient = null;
		try {
			outToClient = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			throw new RuntimeException("Couldn't get socket output stream.");
		}

		if (outToClient != null) {
			try {
				char mode = silent ? SILENT_MODE : VERBAL_MODE;
				outToClient.writeBytes(mode + data + '\n');
				// end with 'enter' to send data
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
