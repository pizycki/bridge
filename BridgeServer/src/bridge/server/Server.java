package bridge.server;

public class Server {

	public static void main(String[] args) throws Exception {
		// First argument is the address of the server.

		int port = 5555; // Default
		if (args != null && args.length > 0)
			port = Integer.parseInt(args[0]);

		System.out.println("Server will be hosted on port " + port);

		Bidding bidding = new Bidding(port);
		bidding.init();
		bidding.start();
	}
}
