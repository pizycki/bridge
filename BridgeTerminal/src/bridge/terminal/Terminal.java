package bridge.terminal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import bridge.server.Commands;

public class Terminal {
	public static void writeLine(String line) {
		System.out.println(line);
	}

	public static void main(String[] args) throws Exception {
		String address;
		int port;
		if (args.length < 2) {
			address = "localhost";
			port = 5555;

		} else {
			// First argument is the port on which socket will be created.
			// Second argument is the address of the server.
			address = args[0];
			port = Integer.parseInt(args[1]);
		}

		final int loopDelay = 200;

		final Status status = new Status();
		status.setConnected(false);
		status.setGameOn(false);
		status.setConnectionError(false);
		status.setYourTurn(false);
		status.setFinished(false);

		final Socket clientSocket;
		try {
			clientSocket = new Socket(address, port);
		} catch (Exception e) {
			throw new RuntimeException("Couldn't create client socket.");
		}

		// Streams
		final DataOutputStream outToServer = new DataOutputStream(
				clientSocket.getOutputStream());
		final BufferedReader inFromServer = new BufferedReader(
				new InputStreamReader(clientSocket.getInputStream()));
		final BufferedReader bufferRead = new BufferedReader(
				new InputStreamReader(System.in));

		final Thread writeThread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!status.isConnectionError() && status.isConnected()
						&& status.isGameOn() && !status.isFinished()) {
					if (status.isYourTurn()) {

						// What's your bid?
						String userInput;
						try {
							writeLine("What's your bid?");
							userInput = bufferRead.readLine();
							// If any user input processing, do it here!
							outToServer.writeBytes(userInput + '\n');
							status.setYourTurn(false);
						} catch (IOException e) {
							e.printStackTrace();
							status.setConnectionError(true);
						}
					}

					// Sleep a bit
					try {
						Thread.sleep(loopDelay);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}, "Write thread");
		// Start when game is on

		final Thread readThread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!status.isConnectionError() && !status.isFinished()) {
					// writeLine("readThread - while - start");
					if (status.isConnected()) {

						// Listen to the server
						String serverResponse = "";
						try {
							serverResponse = inFromServer.readLine();

							// Parse response
							if (serverResponse.isEmpty()) {
								continue;
							}

							// Silent?
							boolean silent = false; // by default 'false'
							if (serverResponse.toCharArray()[0] == 's')
								silent = true;

							// Cut the first letter
							serverResponse = serverResponse.substring(1);
							// If verbose, print it in console
							if (!silent) {
								writeLine(serverResponse);
							}
						} catch (IOException e) {
							e.printStackTrace();

							// TODO check connection
							status.setConnectionError(true);

							throw new RuntimeException("Connection error");
						}

						switch (serverResponse) {
						case Commands.START:
							status.setGameOn(true);
							startThread(writeThread);
							break;
						case Commands.OVER:
							status.setGameOn(false);
							status.setFinished(true);
							try {
								clientSocket.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
							writeLine("GAME OVER");
							break;
						case Commands.TURN:
							status.setYourTurn(true);
							break;
						case Commands.WAIT:
							status.setYourTurn(false);
							writeLine("Waiting for other players...");
							break;
						case Commands.INVALID:
							status.setYourTurn(true);
							writeLine("Your bid request was invalid, try again!");
						default:
							break;
						}
					} else {
						String serverResponse;
						try {
							writeLine("Waiting for server to connect.");
							serverResponse = inFromServer.readLine();
							writeLine(serverResponse.substring(1));
							status.setGameOn(true);
							status.setConnected(true);
						} catch (IOException e) {
							e.printStackTrace();
							status.setConnectionError(true);
						}
					}

					// Sleep a bit
					try {
						Thread.sleep(loopDelay);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}, "Read thread");
		readThread.start();

	}

	static void startThread(Thread thread) {
		thread.start();
	}

}
