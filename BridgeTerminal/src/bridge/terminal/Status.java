package bridge.terminal;

public class Status {
	private boolean connected;
	private boolean gameOn;
	private boolean connectionError;
	private boolean yourTurn;
	private boolean finished;
	
	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isConnectionError() {
		return connectionError;
	}

	public void setConnectionError(boolean connectionError) {
		this.connectionError = connectionError;
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public boolean isGameOn() {
		return gameOn;
	}

	public void setGameOn(boolean isGameOn) {
		this.gameOn = isGameOn;
	}

	public boolean isYourTurn() {
		return yourTurn;
	}

	public void setYourTurn(boolean yourTurn) {
		this.yourTurn = yourTurn;
	}
	

}
