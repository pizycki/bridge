package bridge.server.bids;

import bridge.server.Player;

public abstract class Bid {
	private Player player;

	private String details;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	

	// TODO ADD INFORMATION ABOUT THE BID
	// Pas
	// Kontra
	// 1 trefl
	// 2 kier
	// 1 kier
	// 1 bez atu (BA)

	public abstract int getValue();
	
}

